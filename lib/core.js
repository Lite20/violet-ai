var VioletAI = function(lang, settings) {
    var ai = require('./lang/' + lang + '.js');
    ai.settings = settings;
    return ai;
};

module.exports = VioletAi;
