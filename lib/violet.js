const FLAG = {
    NOUN: 1,
    VERB: 2,
    ADJECTIVE: 3,
    ADVERB: 4,
    PRONOUN: 5,
    PREPOSITION: 6,
    CONJUCTION: 7,
    DETERMINER: 8,
    EXCLAMATION: 9,
    INTERJECTION: 10
};

var VioletAI = function(lang) {
    this.dictionary = require('./lang/' + lang + '.json');
    this.missing = [];
};

VioletAI.prototype.getMask = function(clauseTerms) {
    var mask = [];
    for (var t in clauseTerms) {
        mask.push(this.getFlags(clauseTerms[t]));
    }

    return mask;
};

VioletAI.prototype.getSubject = function(clauseTerms, mask) {
    var verbs = this.getTypeIndeces(FLAG.VERB, mask);
    var primeVerbId = 1;
    return {
        subject: this.getAgent(clauseTerms, mask, verbs[primeVerbId]),
        action: verbs[0]
    };
};

VioletAI.prototype.getAgent = function(clauseTerms, mask, verbIndex) {
    // noun preceeding is likely agent of verb
    if (verbIndex != 0 && mask[verbIndex - 1].includes(FLAG.NOUN)) {
        return verbIndex - 1;
    }

    // pronoun preceeding is likely agent of verb
    if (verbIndex != 0 && mask[verbIndex - 1].includes(FLAG.PRONOUN)) {
        return verbIndex - 1;
    }

    // noun following preposition is likely agent
    if (verbIndex + 1 < mask.length) {
        var followingNounPosition = this.indexOfTypeAfterTypeChain(
            mask,
            verbIndex + 2,
            FLAG.NOUN,
            FLAG.ADJECTIVE
        );

        if (mask[verbIndex + 1].includes(FLAG.PREPOSITION) && followingNounPosition != -1) {
            return followingNounPosition;
        }
    }

    return null;
};

VioletAI.prototype.indexOfTypeAfterTypeChain = function(mask, startIndex, targetType, chainType) {
    for (var i = startIndex; i < mask.length; i++) {
        if (mask[i].includes(targetType)) return i;
        if (mask[i].includes(chainType)) continue;
        else return -1;
    }
};

VioletAI.prototype.getTypeIndeces = function(type, mask) {
    var indeces = [];
    for (var i = 0; i < mask.length; i++) {
        if (mask[i].includes(type)) {
            indeces.push(i);
        }
    }

    return indeces;
};

VioletAI.prototype.isStatement = function(clauseTerms, mask) {
    // TODO write function
    return true;
};

VioletAI.prototype.getFlags = function(term) {
    var flags = [];
    if (this.dictionary[term]) {
        for (var flagId in this.dictionary[term].flags) {
            var flagName = this.dictionary[term].flags[flagId];
            if(FLAG[flagName]) flags.push(FLAG[flagName]);
            else console.log("foul flag " + flagName);
        }
    } else {
        this.missing.push(term);
    }

    return flags;
};

function log(msg) {
    console.log(msg);
}

module.exports = VioletAI;
