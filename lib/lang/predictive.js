var added = 0;
var queue = [];
var dictionary = require('./dict.json');
var structures = require('./struct.json');
/**
    Convenience
**/
const flag = {
    NOUN: 1,
    VERB: 2,
    ADJECTIVE: 3,
    ADVERB: 4,
    PRONOUN: 5,
    PREPOSITION: 6,
    CONJUCTION: 7,
    DETERMINER: 8,
    EXCLAMATION: 9,
    INTERJECTION: 10
};

function parseStatement(statement) {
    let mask = getMask(statement);
    let struc = getClosestStruct(mask);
}

function getClosestStruct(mask) {
    if(structures[mask]) {
        return structures[mask];
    } else {
        // TODO evaluate how far each mask is (1 difference, two differences etc.)
        // and return an array
    }
}

function getFlagID(flagged) {
    return flag[flagged.toUpperCase()] || 0;
}
