const VioletAI = require('../lib/violet.js');
const assert = require('assert');
const chai = require('chai');

const expect = chai.expect;

const ai = new VioletAI('en');
const test1 = "are you ready".split(" ");

describe('Parsing', function() {
    describe('#dictionary', function() {
        it('should be an object', function() {
            expect(ai.dictionary).to.be.a('object');
        });
    });

    describe('#getFlags', function() {
        it('should flag missing words', function() {
            let term = 'gro3nt4ogwn';
            ai.getFlags(term);
            expect(ai.missing.includes(term)).to.equal(true);
        });

        it('should return [] for missing word', function() {
            expect(ai.getFlags('30JGTIORTI')).to.have.lengthOf(0);
        });

        it('should fetch existing words', function() {
            expect(ai.getFlags('dog')).to.have.lengthOf.above(0);
        });
    });

    describe('#getMask', function() {
        it('should return an array', function() {
            let mask = ai.getMask(test1);
            expect(mask).to.be.a('array');
        });
    });

    describe('#getAgent', function() {
        let mask = ai.getMask(test1);
        it('should return "you" to "are you ready"', function() {
            let agentId = ai.getAgent(test1, mask, 2);
            expect(test1[agentId]).to.equal("you");
        });

        it('should return null for no agent', function() {
            // 1 is a noun
            expect(ai.getAgent(['dog'], [1], 0)).to.equal(null);
        });
    });

    describe('#getSubject', function() {
        it('should return "you" to "are you ready"', function() {
            let mask = ai.getMask(test1);
            let subject = ai.getSubject(
                test1,
                mask
            );

            expect(test1[subject.subject]).to.equal('you');
        });
    });
});
