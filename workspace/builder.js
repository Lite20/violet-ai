const fs = require('fs');
const termlist = require('./termlist.json');
const webster = require('./websters_dictionary.json');
const wordnet = require('./wordnett.json').synset;
const patchfile = require('./violet_oldpatch.json');

var dict = require('./base.json');

console.time('compiling');

// large wordbank
console.log('compiling mass wordbank');
for (var term in termlist) {
    addTerm(term);
}

// websters dictionary
console.log('compiling websters');
for (var term in webster) {
    addTerm(term);
    addDef(term, webster[term]);
}

// wordnet dictionary
console.log('compiling wordnet');
const flagMap = {
    'n': 'NOUN',
    'v': 'VERB',
    'a': 'ADJECTIVE',
    's': 'ADJECTIVE',
    'r': 'ADVERB'
};

for (var id in wordnet) {
    var terms = wordnet[id].word;
    for (var termid in terms) {
        var term = terms[termid];
        var usage = flagMap[wordnet[id].pos];

        if(!isNaN(term)) continue;

        addTerm(term);

        if (typeof usage == 'undefined') {
            console.log('unknown flag ' + wordnet[id].pos);
            continue;
        }

        addFlag(term, usage);
    }
}

// old patchfile dictionary
console.log('compiling old patchfile');

const flagMap2 = {
    NOUN: 1,
    VERB: 2,
    ADJECTIVE: 3,
    ADVERB: 4,
    PRONOUN: 5,
    PREPOSITION: 6,
    CONJUCTION: 7,
    DETERMINER: 8,
    EXCLAMATION: 9,
    INTERJECTION: 10
};

for(var term in patchfile) {
    addTerm(term);
    var flag;
    for(var flagid in patchfile[term]) {
        flag = patchfile[term][flagid].toUpperCase();
        flag = flag.split(",")[0];
        if(flagMap2[flag]) addFlag(term, flag);
    }
}

console.timeEnd('compiling');
console.log('writing dictionary...');
fs.writeFile('dict.json', JSON.stringify(dict), 'utf8', function() {
    console.log('dictionary compiled.');
});

function addTerm(term) {
    term = term.toLowerCase();
    if (!dict[term]) {
        dict[term] = {};
        // console.log('+term ' + term);
    }
}

function addDef(term, def) {
    term = term.toLowerCase();
    if (!dict[term].def) {
        dict[term].def = def;
        // console.log('+def ' + term);
    }
}

function addFlag(term, flag) {
    term = term.toLowerCase();
    if (!dict[term].flags) dict[term].flags = [flag];
    else if(!dict[term].flags.includes(flag)) {
        dict[term].flags.push(flag);
    }
    // console.log('+flags ' + term);
}
