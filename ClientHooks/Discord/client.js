require("colors");
var Violet = require("../../index.js");
var DiscordClient = require('discord.io');
var bot = new DiscordClient({
    email: "",
    password: "",
    autorun: true
});

bot.on('ready', function() {
    console.log(bot.username + " - (" + bot.id + ")");
});

bot.on('message', function(user, userID, channelID, message, rawEvent) {
    if (message === "ping") {
        bot.sendMessage({
            to: channelID,
            message: "pong"
        });
    }
});

function tag() {
    return "[LightBot]".blue;
}